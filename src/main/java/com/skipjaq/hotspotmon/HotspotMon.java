package com.skipjaq.hotspotmon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * @author Rob Harrop
 */
@SpringBootApplication
public class HotspotMon {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(HotspotMon.class, args);
    }

}
