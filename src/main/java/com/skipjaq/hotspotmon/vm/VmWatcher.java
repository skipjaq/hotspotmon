package com.skipjaq.hotspotmon.vm;

import reactor.core.publisher.Flux;

import java.util.concurrent.ConcurrentMap;

/**
 * 
 * @author Rob Harrop
 */
public interface VmWatcher {

    Flux<Vm> watchLocal();

    ConcurrentMap<Integer, Vm> activeVms();
}
