package com.skipjaq.hotspotmon.influxdb;

import com.skipjaq.hotspotmon.sampler.SampleData;
import com.skipjaq.hotspotmon.sampler.Sampler;
import org.influxdb.InfluxDB;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Publishes sampled metrics to InfluxDB using the {@link InfluxDB} HTTP client.
 * <p>
 * Samples are sent to InfluxDB in batches. Batch have a maximum size set by
 * {@link InfluxDBConfigurationProperties#batchSize} and a maximum collection duration set
 * by {@link InfluxDBConfigurationProperties#batchSeconds}.
 *
 * @author Rob Harrop
 */
class InfluxDBPublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfluxDBPublisher.class);

    private final Sampler sampler;

    private final InfluxDB influxDb;

    private final InfluxDBConfigurationProperties config;

    private final Scheduler scheduler;

    /**
     * Creates a new a <code>InfluxDBPublisher</code> that samples metrics from the supplied
     * {@link Sampler} and publishes them via the supplied {@link InfluxDB} client.
     *
     * @param sampler  the <code>Sampler</code> to read metrics from
     * @param influxDb the <code>InfluxDB</code> to send metrics through
     * @param config   the configuration for publication
     */
    InfluxDBPublisher(Sampler sampler, InfluxDB influxDb, InfluxDBConfigurationProperties config) {
        this.sampler = sampler;
        this.influxDb = influxDb;
        this.config = config;
        this.scheduler = Schedulers.newSingle("influxdb", false);
    }

    @PostConstruct
    public void start() {
        this.sampler.sampler()
                .map(this::sampleToPoint)
                .buffer(this.config.getBatchSize(), Duration.of(this.config.getBatchSeconds(), ChronoUnit.SECONDS))
                .map(this::makeBatch)
                .onBackpressureDrop(b -> LOGGER.warn("Dropping batch: {}", b))
                .doOnNext(s -> LOGGER.trace("Publishing batch: {}"))
                .publishOn(this.scheduler)
                .doOnComplete(this::onPublishComplete)
                .subscribe(new Subscriber<BatchPoints>() {
                    Subscription s;

                    @Override
                    public void onSubscribe(Subscription s) {
                        this.s = s;
                        s.request(1);
                    }

                    @Override
                    public void onNext(BatchPoints batchPoints) {
                        influxDb.write(batchPoints);
                        s.request(1);
                    }

                    @Override
                    public void onError(Throwable t) {
                        LOGGER.error("InfluxDB Publisher Failed", t);
                    }

                    @Override
                    public void onComplete() {
                        LOGGER.info("InfluxDB Publisher Completed");
                    }
                });
    }

    protected void onPublishComplete() {
       // no-op
    }

    private BatchPoints makeBatch(List<Point> points) {
        return BatchPoints.database(this.config.getDatabase())
                .consistency(InfluxDB.ConsistencyLevel.ANY)
                .retentionPolicy("default")
                .points(points.toArray(new Point[points.size()]))
                .build();
    }

    private Point sampleToPoint(SampleData sampleData) {
        return Point.measurement(sampleData.getMetricName())
                .time(sampleData.getSampleTime(), TimeUnit.MILLISECONDS)
                .addField("value", sampleData.getSampleValue())
                .addField("pid", sampleData.getPid())
                .build();
    }

}