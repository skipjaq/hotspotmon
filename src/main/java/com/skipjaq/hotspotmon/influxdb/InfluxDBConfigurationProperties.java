package com.skipjaq.hotspotmon.influxdb;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * {@link ConfigurationProperties} for InfluxDB publication subsystem.
 *
 * @author Rob Harrop
 */
@ConfigurationProperties(prefix = "influxdb")
@Component
@Data
class InfluxDBConfigurationProperties {

    /**
     * The URL of the InfluxDB server HTTP endpoint
     */
    private String url;

    /**
     * The username used to authenticate against InfluxDB
     */
    private String username;

    /**
     * The password used to authenticate against InfluxDB
     */
    private String password;

    /**
     * The InfluxDB database where metrics are stored
     */
    private String database;

    /**
     * The maximum size of each batch of metrics sent to InfluxDB
     */
    private int batchSize;

    /**
     * The maximum number of seconds to wait between sending each batch
     */
    private int batchSeconds;
}
