package com.skipjaq.hotspotmon.influxdb;

import com.skipjaq.hotspotmon.sampler.Sampler;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Rob Harrop
 */
@Configuration
class InfluxDBConfiguration {

    @Bean
    public InfluxDB influxDb(InfluxDBConfigurationProperties props) {
        return InfluxDBFactory.connect(props.getUrl(), props.getUsername(), props.getPassword());
    }

    @Bean
    public InfluxDBPublisher influxDBPublisher(Sampler sampler, InfluxDB influxDB, InfluxDBConfigurationProperties props) {
        return new InfluxDBPublisher(sampler, influxDB, props);
    }
}
