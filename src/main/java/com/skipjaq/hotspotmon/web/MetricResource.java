package com.skipjaq.hotspotmon.web;

import com.skipjaq.hotspotmon.vm.Vm;
import org.springframework.hateoas.ResourceSupport;

import static com.skipjaq.hotspotmon.web.Links.toMetrics;
import static com.skipjaq.hotspotmon.web.Links.toVm;

/**
 * @author Rob Harrop
 */
class MetricResource extends ResourceSupport {

    private final Vm vm;

    private final String name;

    public MetricResource(Vm vm, String name) {
        this.vm = vm;
        this.name = name;
        add(toMetrics(vm).withRel("metrics"));
        add(toVm(vm).withRel("vm"));
    }

    public String getName() {
        return name;
    }
}
