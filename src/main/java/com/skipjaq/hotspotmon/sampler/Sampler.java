package com.skipjaq.hotspotmon.sampler;

import reactor.core.publisher.Flux;

/**
 * A sampler that streams sample data out to subscribers.
 * <p>
 * Samplers may expose for a single VM, but the intention is that a single
 * sampler is multiplexed across many VMs and many metrics.
 *
 * @author Rob Harrop
 */
public interface Sampler {

    /**
     * Creates a {@link Flux} that publishes the sample data from this sampler.
     *
     * @return the sampling <code>Flux</code>
     */
    Flux<SampleData> sampler();
}
