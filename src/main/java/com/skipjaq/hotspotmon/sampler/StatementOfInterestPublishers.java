package com.skipjaq.hotspotmon.sampler;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;

import java.time.temporal.ChronoUnit;
import java.util.Properties;

/**
 * Publishers to expose incoming {@link StatementOfInterest statements of interest}.
 * <p>
 * The {@link #vmArgsPublisher(VmWatcher)} watches for VM start events and publishes
 * the statements of interest expressed as system properties on each VM.
 *
 * Users can signal the statements of interest for a VM by setting system properties
 * of the form <code>com.skipjaq.hotspotmon.&lt;metric_name&gt;=&lt;sample_millis&gt;</code>
 * when starting that VM.
 *
 * @author Rob Harrop
 */
final class StatementOfInterestPublishers {

    private static final Logger LOGGER = LoggerFactory.getLogger(StatementOfInterestPublishers.class);

    /**
     * Prefix for statement of interest system properties.
     */
    static final String HOTSPOTMON_PROPERTY_PREFIX = "com.skipjaq.hotspotmon.";

    /**
     * Creates a {@link Flux} that exposes a {@link StatementOfInterest} for each
     * matching system property on each started VM.
     *
     * @param watcher the <code>VmWatcher</code> used to track VM start events.
     * @return the <code>StatementOfInterest</code> publisher.
     */
    static Flux<StatementOfInterest> vmArgsPublisher(VmWatcher watcher) {
        return watcher.watchLocal().concatMap(StatementOfInterestPublishers::extractVmStatementsOfInterest);
    }

    private static Flux<StatementOfInterest> extractVmStatementsOfInterest(Vm vm) {
        Properties properties = SamplerUtils.propertiesFromArgString(vm.getVmArgs());
        return Flux.fromStream(properties.stringPropertyNames()
                .stream()
                .filter(name -> name.startsWith(HOTSPOTMON_PROPERTY_PREFIX))
                .map(name -> {
                    String value = properties.getProperty(name);
                    long millis = Long.parseLong(value);
                    String metricName = name.substring(HOTSPOTMON_PROPERTY_PREFIX.length());
                    return new StatementOfInterest(vm.getPid(), metricName, millis, ChronoUnit.MILLIS);
                }))
                .doOnNext(s -> LOGGER.info("SOI {}", s));
    }
}
