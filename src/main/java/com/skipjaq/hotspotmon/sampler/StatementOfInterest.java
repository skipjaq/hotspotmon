package com.skipjaq.hotspotmon.sampler;

import lombok.Data;

import java.time.temporal.ChronoUnit;

/**
 * Expresses interest in receiving samples of a metric from a JVM.
 * <p>
 * The JVM is identified by the {@link #getPid()} and the metric is
 * identified by {@link #getMetricName() name}. Each statement of interest
 * has an accompanying specification for how often samples should be taken.
 *
 * @author Rob Harrop
 */
@Data
class StatementOfInterest {

    private final Integer pid;

    private final String metricName;

    private final long samplePeriod;

    private final ChronoUnit sampleUnit;

    StatementOfInterest(Integer pid, String metricName, long samplePeriod, ChronoUnit sampleUnit) {
        this.pid = pid;
        this.metricName = metricName;
        this.samplePeriod = samplePeriod;
        this.sampleUnit = sampleUnit;
    }
}
