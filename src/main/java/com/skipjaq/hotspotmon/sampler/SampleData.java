package com.skipjaq.hotspotmon.sampler;

import lombok.Data;

/**
 * The data for a single sample of a single metric in a single VM.
 * <p>
 * The {@link #getPid() PID} is the PID of the VM from which the sample was taken,
 * the {@link #getMetricName() name} is the name of the sampled metric, the
 * {@link #getSampleTime() sample time} is the time, in milliseconds, when the sample was
 * taken, and the {@link #getSampleValue() sample value} is the value that was sampled.
 *
 * @author Rob Harrop
 */
@Data
public final class SampleData {

    private final int pid;

    private final String metricName;

    private final long sampleTime;

    private final Number sampleValue;

    public SampleData(int pid, String metricName, long sampleTime, Number sampleValue) {
        this.pid = pid;
        this.metricName = metricName;
        this.sampleTime = sampleTime;
        this.sampleValue = sampleValue;
    }
}
