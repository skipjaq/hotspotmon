package com.skipjaq.hotspotmon.sampler;

import java.util.Objects;
import java.util.Properties;

/**
 * Simple utility methods for the {@link Sampler}.
 *
 * @author Rob Harrop
 */
final class SamplerUtils {

    /**
     * Extracts properties following the standard Java system property specification
     * from a JVM arguments string.
     * <p>
     * Properties are specified using the <code>-D&lt;key&gt;=&lt;value&gt;</code>
     * format. Each key/value pair found in the argument string is added as a property.
     *
     * @param args the arguments supplied to a JVM.
     * @return the properties specified in the supplied argument string.
     */
    static Properties propertiesFromArgString(String args) {
        Objects.requireNonNull(args);
        String[] argArray = args.split("\\s");
        Properties properties = new Properties();
        for (String arg : argArray) {
            if (arg.startsWith("-D")) {
                String[] parts = arg.split("=");
                if (parts.length == 2) {
                    properties.setProperty(parts[0].trim().substring(2), parts[1].trim());
                }
            }
        }
        return properties;
    }
}
