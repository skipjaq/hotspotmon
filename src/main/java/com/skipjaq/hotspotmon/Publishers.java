package com.skipjaq.hotspotmon;

import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.temporal.TemporalUnit;
import java.util.function.Supplier;

/**
 * @author Rob Harrop
 */
public class Publishers {

    public static Flux<Long> tick(long period, TemporalUnit unit) {
        return tick(System::currentTimeMillis, period, unit);
    }

    static Flux<Long> tick(Supplier<Long> timeSampler, long period, TemporalUnit unit) {
        return Flux.interval(Duration.of(period, unit)).onBackpressureDrop().map(i -> timeSampler.get());
    }
}
