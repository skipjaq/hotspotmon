package com.skipjaq.hotspotmon.sampler;

import com.skipjaq.hotspotmon.vm.Vm;
import com.skipjaq.hotspotmon.vm.VmWatcher;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.test.TestSubscriber;
import reactor.core.tuple.Tuple;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Rob Harrop
 */
public class ReactiveSamplerTests {

    private VmWatcher watcher;

    @Before
    public void before() {
        this.watcher = mock(VmWatcher.class);
    }

    @Test
    public void sampleSingleVm() {
        long dummyTime = System.currentTimeMillis();

        Vm vm = mock(Vm.class);
        when(vm.getPid()).thenReturn(123);
        when(vm.sampleMetric("test", 50, ChronoUnit.MILLIS)).thenReturn(Flux.just(Tuple.of(dummyTime, 789)));

        when(this.watcher.watchLocal()).thenReturn(Flux.just(vm));

        Flux<StatementOfInterest> soiFlux = Flux.just(new StatementOfInterest(123, "test", 50, ChronoUnit.MILLIS));

        ReactiveSampler sampler = new ReactiveSampler(this.watcher, soiFlux);
        TestSubscriber.subscribe(sampler.sampler()).awaitAndAssertNextValuesWith(
                validator(123, dummyTime, "test", 789));
    }

    @Test
    public void sampleMultiVm() {
        long dummyTime = System.currentTimeMillis();

        Vm vm = mock(Vm.class);
        when(vm.getPid()).thenReturn(123);
        when(vm.sampleMetric("test", 50, ChronoUnit.MILLIS)).thenReturn(Flux.just(Tuple.of(dummyTime, 789)));

        Vm vm2 = mock(Vm.class);
        when(vm2.getPid()).thenReturn(124);
        when(vm2.sampleMetric("test2", 50, ChronoUnit.MILLIS)).thenReturn(Flux.just(Tuple.of(dummyTime, 345), Tuple.of(dummyTime + 100, 456)));

        when(this.watcher.watchLocal()).thenReturn(Flux.just(vm, vm2));

        Flux<StatementOfInterest> soiFlux = Flux.just(new StatementOfInterest(123, "test", 50, ChronoUnit.MILLIS), new StatementOfInterest(124, "test2", 50, ChronoUnit.MILLIS));

        ReactiveSampler sampler = new ReactiveSampler(this.watcher, soiFlux);

        TestSubscriber.subscribe(sampler.sampler()).awaitAndAssertNextValuesWith(
                validator(123, dummyTime, "test", 789),
                validator(124, dummyTime, "test2", 345),
                validator(124, dummyTime + 100, "test2", 456));
    }

    private Consumer<SampleData> validator(int pid, long time, String metricName, Number sampleValue) {
        return sd -> {
            assertEquals(pid, sd.getPid());
            assertEquals(time, sd.getSampleTime());
            assertEquals(metricName, sd.getMetricName());
            assertEquals(sampleValue, sd.getSampleValue());
        };
    }

}
