package com.skipjaq.hotspotmon.vm.jvmstat;

import com.skipjaq.hotspotmon.TestApp;
import com.skipjaq.hotspotmon.vm.Vm;
import org.junit.Assert;
import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.core.test.TestSubscriber;
import reactor.core.tuple.Tuple2;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.skipjaq.hotspotmon.TestApp.withTestApp;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Rob Harrop
 */
public class JvmstatVmWatcherTests {

    private final JvmstatVmWatcher watcher = new JvmstatVmWatcher();

    @Test
    public void testWatchLocal() throws Exception {
        Flux<Vm> flux = this.watcher.watchLocal();
        withTestApp(sample -> {
            TestSubscriber.subscribe(flux.filter(s -> s.getMainClass().equals(TestApp.class.getSimpleName())))
                    .awaitAndAssertNextValuesWith(vmShadow -> assertEquals(vmShadow.getPid(), sample.getPid()));
        });
    }

    @Test
    public void testActiveVms() throws Exception {
        withTestApp(sample -> {
            // wait for a subscriber to see the events
            TestSubscriber.subscribe(this.watcher.watchLocal()).awaitAndAssertNextValuesWith(Assert::assertNotNull);

            // the watcher should have tracked its internal state now
            assertNotNull(this.watcher.activeVms().get(sample.getPid()));
        });
    }
}
