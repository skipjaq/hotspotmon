package com.skipjaq.hotspotmon;

import org.junit.Test;
import org.reactivestreams.Publisher;
import reactor.core.test.TestSubscriber;

import java.time.temporal.ChronoUnit;
import java.util.function.Consumer;

import static org.junit.Assert.assertTrue;

/**
 * @author Rob Harrop
 */
public class PublishersTests {

    @Test
    public void testTick() {
        long dummy = 123;
        Publisher<Long> publisher = Publishers.tick(() -> dummy, 50, ChronoUnit.MILLIS);
        TestSubscriber.subscribe(publisher).awaitAndAssertNextValues(dummy, dummy, dummy);
    }

    @Test
    public void testSystemTick() {
        Consumer<Long> assertion = (l) -> assertTrue(l < System.currentTimeMillis());
        TestSubscriber.subscribe(Publishers.tick(50, ChronoUnit.MILLIS)).awaitAndAssertNextValuesWith(assertion, assertion, assertion);
    }
}
