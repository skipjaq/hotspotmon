package com.skipjaq.hotspotmon;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Rob Harrop
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HotpostMonTests {

    @Test
    public void contextLoads() {
    }

}
