package com.skipjaq.hotspotmon;

import java.lang.management.ManagementFactory;

/**
 * @author Rob Harrop
 */
public class TestUtils {

    /**
     * Gets the process ID of the running process.
     */
    public static int getPid() {
        String name = ManagementFactory.getRuntimeMXBean().getName();
        return Integer.parseInt(name.split("@")[0]);
    }
}
