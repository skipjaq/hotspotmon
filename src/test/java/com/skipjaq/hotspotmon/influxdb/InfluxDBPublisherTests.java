package com.skipjaq.hotspotmon.influxdb;

import com.skipjaq.hotspotmon.sampler.SampleData;
import com.skipjaq.hotspotmon.sampler.Sampler;
import org.hamcrest.Matchers;
import org.influxdb.InfluxDB;
import org.influxdb.dto.BatchPoints;
import org.junit.Before;
import org.junit.Test;
import reactor.core.publisher.Flux;

import java.util.concurrent.CountDownLatch;

import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Rob Harrop
 */
public class InfluxDBPublisherTests {

    public static final int BATCH_SIZE = 5;
    public static final int BATCH_SECONDS = 1;

    private Sampler mockSampler;

    private InfluxDB mockClient;

    @Before
    public void before() {

        this.mockSampler = mock(Sampler.class);

        this.mockClient = mock(InfluxDB.class);
    }

    private Flux<SampleData> sampleData(int count) {
        return Flux.range(0, count).map(x -> new SampleData(123, "test", System.currentTimeMillis(), x));
    }

    @Test
    public void testPublishUnderBatchSize() throws InterruptedException {
        doTest(BATCH_SIZE - 1);
    }

    @Test
    public void testPublishOverBatchSize() throws InterruptedException {
        doTest(BATCH_SIZE);
    }

    private void doTest(int count) throws InterruptedException {
        InfluxDBConfigurationProperties props = new InfluxDBConfigurationProperties();
        props.setBatchSize(BATCH_SIZE);
        props.setBatchSeconds(BATCH_SECONDS);
        props.setDatabase("testdb");

        // setup a sampler with only 4 samples, this should trigger the batch timeout
        when(this.mockSampler.sampler()).thenReturn(sampleData(count));

        // we need to latch on completion of the source data set otherwise, verify might happen
        // before we've actually had a chance to do anything with publisher
        CountDownLatch latch = new CountDownLatch(1);

        InfluxDBPublisher publisher = new InfluxDBPublisher(this.mockSampler, this.mockClient, props) {
            @Override
            protected void onPublishComplete() {
                latch.countDown();
            }
        };
        publisher.start();

        // wait for all data to be drained
        latch.await();

        // now, we can verify
        verify(this.mockClient).write(argThat(Matchers.notNullValue(BatchPoints.class)));
    }
}
